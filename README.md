# Pytube

pytube is a lightweight library written in Python. It has no third party dependencies and aims to be highly reliable.

pytube also makes pipelining easy, allowing you to specify callback functions for different download events, such as on progress or on complete.

Finally pytube also includes a command-line utility, allowing you to quickly download videos right from terminal.

Download using pip via pypi

pip install pytube