from pytlube import YouTube

print("Enter the video link")
#link of the video to be downloaded
you_link = input()              

#object creation using YouTube
ytl = YouTube(you_link)

#getting all the possible video quality options
video_quality = ytl.get_videos()

ytl.set_filename('demoVideo1') #to set the name of the file
num=1

#printing all the possible video quality options
for i in video_quality:
    print(str(num)+" "+str(i))
    num+=1
    
print("Enter the number of the video: ")
n= int(input())

selected_vid=video_quality[n-1]

print("Enter the destination: ")
destination_path = input()

#downloading the video
selected_vid.download(destination_path)

print(ytl.filename+" has been successfully downloaded")
